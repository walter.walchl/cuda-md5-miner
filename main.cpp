#include "utility.h"

#include <cstdio>
#include <ctime>


int main(int argc, char **argv){
	const char *str ;
	if( argc > 1 )
		str = argv[1] ;
	else
		str = argv[0] ;

	char M[512/8] ;
	uint8_t md5[128/8] ;

	prepare_string( M, str ) ;

	// find
	auto time_begin = clock() ;
	*(uint32_t*)M = find_min_accepted( (const uint32_t*)M ) ;
	auto duration = (clock()-time_begin) / (double)CLOCKS_PER_SEC ;
	printf("Duration: %.3f s\n", duration ) ;

	md5_sum( (uint32_t*)md5, (const uint32_t*)M ) ;

	for( int i=0 ; i<128/8 ; i++ )
		printf(" %02x", md5[i] ) ;
	printf("\n") ;

	return 0 ;
}

