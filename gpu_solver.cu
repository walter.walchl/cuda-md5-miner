#include "utility.h"

#include <cstdio>
#include <cstring>

__device__ const uint32_t s[64] = {
	  7, 12, 17, 22,  7, 12, 17, 22,  7, 12, 17, 22,  7, 12, 17, 22,
	  5,  9, 14, 20,  5,  9, 14, 20,  5,  9, 14, 20,  5,  9, 14, 20,
	  4, 11, 16, 23,  4, 11, 16, 23,  4, 11, 16, 23,  4, 11, 16, 23,
	  6, 10, 15, 21,  6, 10, 15, 21,  6, 10, 15, 21,  6, 10, 15, 21 } ;

__device__ const uint32_t K[64] = {
		0xd76aa478, 0xe8c7b756, 0x242070db, 0xc1bdceee,
		0xf57c0faf, 0x4787c62a, 0xa8304613, 0xfd469501,
		0x698098d8, 0x8b44f7af, 0xffff5bb1, 0x895cd7be,
		0x6b901122, 0xfd987193, 0xa679438e, 0x49b40821,
		0xf61e2562, 0xc040b340, 0x265e5a51, 0xe9b6c7aa,
		0xd62f105d, 0x02441453, 0xd8a1e681, 0xe7d3fbc8,
		0x21e1cde6, 0xc33707d6, 0xf4d50d87, 0x455a14ed,
		0xa9e3e905, 0xfcefa3f8, 0x676f02d9, 0x8d2a4c8a,
		0xfffa3942, 0x8771f681, 0x6d9d6122, 0xfde5380c,
		0xa4beea44, 0x4bdecfa9, 0xf6bb4b60, 0xbebfbc70,
		0x289b7ec6, 0xeaa127fa, 0xd4ef3085, 0x04881d05,
		0xd9d4d039, 0xe6db99e5, 0x1fa27cf8, 0xc4ac5665,
		0xf4292244, 0x432aff97, 0xab9423a7, 0xfc93a039,
		0x655b59c3, 0x8f0ccc92, 0xffeff47d, 0x85845dd1,
		0x6fa87e4f, 0xfe2ce6e0, 0xa3014314, 0x4e0811a1,
		0xf7537e82, 0xbd3af235, 0x2ad7d2bb, 0xeb86d391 } ;

__device__ const uint32_t
	a0 = 0x67452301,
	b0 = 0xefcdab89,
	c0 = 0x98badcfe,
	d0 = 0x10325476 ;


#define LEFTROTATE(x,c)	(((x)<<(c))|((x)>>(32-(c))))

// get md5 from 512-bit M
__device__ void md5_sum( uint32_t *md5, const uint32_t *M, const uint32_t key ){
	uint32_t
		A = a0,
		B = b0,
		C = c0,
		D = d0 ;

	for( int i=0 ; i<64 ; i++ ){
		uint32_t F, g ;

		if( i<16 ){
			F = (B & C) | ((~B) & D) ;
			g = i ;
		}
		else if ( i<32 ){
			F = (D & B) | ((~D) & C) ;
			g = (5*i + 1) % 16 ;
		}
		else if ( i<48 ){
			F = B ^ C ^ D ;
			g = (3*i + 5) % 16 ;
		}
		else{
			F = C ^ (B | (~D)) ;
			g = (7*i) % 16 ;
		}
		F += A + K[i] ;
		if( g )
			F += M[g] ;
		else
			F += key ;
		A = D ;
		D = C ;
		C = B ;
		B += LEFTROTATE(F, s[i]) ;
	}

	md5[0] = a0 + A ;
	md5[1] = b0 + B ;
	md5[2] = c0 + C ;
	md5[3] = d0 + D ;
}


__global__ void kernel( uint32_t *min_key, int *max_zeros, const uint32_t* __restrict__ M, uint32_t bias ){
	uint32_t key = bias + blockDim.x*blockIdx.x + threadIdx.x ;

	uint64_t md5[128/64] ;
	md5_sum( (uint32_t*)md5, M, key ) ;

	int zero_count = 0 ;
	if( md5[0] == 0 ){
		zero_count += 64 ;
		uint64_t v = md5[1] ;
		for( int i=0 ; i<64 ; i++ ){
			if( v&1 )
				break ;

			zero_count ++ ;
			v /= 2 ;
		}
	}
	else{
		uint64_t v = md5[0] ;
		for( int i=0 ; i<64 ; i++ ){
			if( v&1 )
				break ;

			zero_count ++ ;
			v /= 2 ;
		}
	}


	if( zero_count >= ACCEPTED_ZEROS )
		atomicMin( min_key, key ) ;
//	if( *max_zeros < zero_count )
	atomicMax( max_zeros, zero_count ) ;
}


// find minimal prefix make the hash accepted
uint32_t find_min_accepted( const uint32_t *_M ){
	uint32_t *M ;
	cudaMalloc( &M, 512/8 ) ;
	cudaMemcpy( M, _M, 512/8, cudaMemcpyDefault ) ;

	uint32_t *min_key ;
	cudaMalloc( &min_key, sizeof(uint32_t) ) ;

	int *max_zeros ;
	cudaMalloc( &max_zeros, sizeof(int) ) ;

	for( uint64_t bias=0 ; bias<=UINT32_MAX ; bias+=BATCH_SIZE ){
		printf("Processing Batch %u~%u", (uint32_t)bias, (uint32_t)bias+BATCH_SIZE) ;

		uint32_t key = UINT32_MAX ;
		cudaMemcpy( min_key, &key, sizeof(key), cudaMemcpyDefault ) ;

		int threads = THREADS_PER_BLOCK ;
		kernel <<< BATCH_SIZE/threads, threads >>> ( min_key, max_zeros, M, (uint32_t)bias ) ;

		cudaMemcpy( &key, min_key, sizeof(key), cudaMemcpyDefault ) ;

		int max ;
		cudaMemcpy( &max, max_zeros, sizeof(max), cudaMemcpyDefault ) ;
		printf("    (MAX ZEROS: %d)\n", max) ;

		if( key < UINT32_MAX )
			return key ;
	}

	return 0 ;
}

