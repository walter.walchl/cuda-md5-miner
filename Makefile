CXX = g++
NVCC = nvcc

CXXFLAGS += -O3 -std=c++11


all: cpu gpu

cpu: main.cpp utility.cpp cpu_solver.cpp
	$(CXX) $(CXXFLAGS) -o $@ $^

gpu: main.cpp utility.cpp gpu_solver.cu
	$(NVCC) $(CXXFLAGS) -o $@ $^

clean:
	rm -f cpu gpu
