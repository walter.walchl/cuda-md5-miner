#ifndef __UTILITY_H__
#define __UTILITY_H__

#include <cinttypes>

#define PREFIX	32

// md5 has >= ACCEPTED_ZEROS 0s, searching is done
#define ACCEPTED_ZEROS	26
//#define ACCEPTED_ZEROS	64

// batch size
#define BATCH_SIZE	((5*32)<<14)
#define THREADS_PER_BLOCK	32

// prepare the input M[512/8]
void prepare_string( char *M, const char *str ) ;

// Count leading zeros
int count_zeros( const uint64_t *md5 ) ;

// get md5 from 512-bit M
void md5_sum( uint32_t *md5, const uint32_t *M ) ;

// find minimal prefix make the hash accepted
uint32_t find_min_accepted( const uint32_t *M ) ;

#endif

