#include "utility.h"

#include <cstdio>


// find minimal prefix make the hash accepted
uint32_t find_min_accepted( const uint32_t *M ){
	uint32_t *prefix = (uint32_t*) M ;
	int max_zeros = 0 ;

	for( uint64_t bias=0 ; bias<=UINT32_MAX ; bias+=BATCH_SIZE ){
		printf("Processing Batch %u~%u", (uint32_t)bias, (uint32_t)bias+BATCH_SIZE) ;
		uint32_t i ;
		for( i=0 ; i<BATCH_SIZE ; i++ ){
			*prefix = bias+i ;

			uint64_t md5[128/64] ;
			md5_sum( (uint32_t*)md5, M ) ;

			int zero_count = count_zeros(md5) ;

			if( max_zeros < zero_count )
				max_zeros = zero_count ;

			if( max_zeros >= ACCEPTED_ZEROS )
				break ;
		}
		printf("    (MAX ZEROS: %d)\n", max_zeros) ;

		if( i < BATCH_SIZE )
			break ;
	}

	return *prefix ;
}

